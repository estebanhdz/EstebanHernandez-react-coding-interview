import { IPerson } from '@lib/models/person';
import { useCallback, useEffect, useState } from 'react';
import { contactsClient } from '@lib/clients/contacts';

export function useContactEdit(id: string) {
  const [contact, setContact] = useState<IPerson | null>(null);

  useEffect(() => {
    const fetchContact = async() => {
      try {
        const fetchedContact = await contactsClient.getContactById(id);
        setContact(fetchedContact);
      } catch (error) {
        console.error(`Error fetching contact with id ${id}`, error);
      }
    }

    fetchContact();
  }, [id]);

  const update = useCallback(async(updated: IPerson) => {
    try {
      await contactsClient.updateContact(id, updated);
    } catch (error) {
      console.error(`Error updating contact with id ${id}`, error);
    }
  }, [id])

  return {
    contact,
    update,
  };
}
