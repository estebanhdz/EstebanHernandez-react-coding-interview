import { useNavigate } from 'react-router-dom';
import { Stack, TextField, Button } from '@mui/material';
import { useFormik } from 'formik';
import * as yup from 'yup';

import { Card } from '@components/atoms';
import { IPerson } from '@lib/models/person';

export interface IContactEditCardProps {
  person: IPerson;
  onUpdate(contact: IPerson): void;
}

const contactValidationSchema = yup.object({
  firstName: yup
    .string()
    .required('First Name is required'),
  lastName: yup
    .string()
    .required('Last Name is required'),
  email: yup
    .string()
    .email('Enter a valid email')
    .required('Last Name is required'),
});

export const ContactEditCard: React.FC<IContactEditCardProps> = ({person, onUpdate}) => {
  const navigate = useNavigate();
  
  const formik = useFormik({
    initialValues: {
      firstName: person.firstName,
      lastName: person.lastName,
      email: person.email,
    },
    validationSchema: contactValidationSchema,
    onSubmit: (values) => {
      onUpdate({
        id: person.id,
        firstName: values.firstName,
        lastName: values.lastName,
        email: values.email,
      });

      navigate('/contacts');
    }
  });

  return (
    <Card>
      <form onSubmit={formik.handleSubmit}>
        <Stack spacing={2} direction="row" sx={{marginBottom: 2}}>
          <TextField
            label="First Name"
            name="firstName"
            fullWidth
            value={formik.values.firstName}
            onChange={formik.handleChange}
            error={formik.touched.firstName && Boolean(formik.errors.firstName)}
            helperText={formik.touched.firstName && formik.errors.firstName}
          />
          <TextField
            label="Last Name"
            name="lastName"
            fullWidth
            value={formik.values.lastName}
            onChange={formik.handleChange}
            error={formik.touched.lastName && Boolean(formik.errors.lastName)}
            helperText={formik.touched.lastName && formik.errors.lastName}
          />
        </Stack>
        <TextField
            label="Email"
            name="email"
            fullWidth
            value={formik.values.email}
            onChange={formik.handleChange}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
          />
          <Button type="submit" variant="contained" fullWidth sx={{marginTop: 2}}>
            Save
          </Button>
      </form>
    </Card>
  );
};
