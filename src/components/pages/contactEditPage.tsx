import { ContactEditCard } from "@components/molecules";
import { useContactEdit } from "@hooks/contacts/useContactEdit";
import { Box } from "@mui/material";
import { useParams } from "react-router-dom";

export const ContactEditPage: React.FC = () => {
  const { id } = useParams();
  const { contact, update } = useContactEdit(id);

  if (! contact) {
    return null;
  }
    
  return (
    <Box p={4} overflow="auto">
      <ContactEditCard person={contact} onUpdate={update} />
    </Box>
    
  );
}

